<h2>Todo/Task management App with MERN stack</h2>
<br/>
<p>A little project I developed using React, Express.js, Node.js, MongoDB</p>
<p>Features:</p>
<ul>
<li>Authentication</li>
<li>Team/tasks CRUD</li>
<li>Admin privilegies</li>
<li>Searching/invitation system</li>
<li>Account security using JWT</li>
</ul>