import "./App.css";
import { Fragment } from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import Navbar from "./sidebar/components/Navbar";
import Search from "./header/search/Search";
import Profile from "./header/login/Profile";

import { useSelector } from "react-redux";

import Projects from "./projects/pages/Projects";
import Login from "./login/pages/Login";
import Teams from "./teams/pages/Teams";
import BasicHeader from "./UI/BasicHeader";
import Overview from "./overview/pages/Overview";
import Settings from "./settings/pages/Settings";

function App() {
  const { user } = useSelector((state) => state.auth);

  return (
    <Fragment>
      <Switch>
        <Route path="/app">
          <nav>
            <aside>
              <Navbar />
            </aside>
          </nav>
          <div className="app-wrapper">
            <header className="header-wrapper">
              <Search />
              <Profile />
            </header>
            <main>
              <Switch>
                <Route path="/app/home">
                  <Overview />
                </Route>
                <Route path="/app/stats">
                  <h2>Feature not ready yet :(</h2>
                </Route>
                <Route path="/app/tasks">
                  <Projects />
                </Route>
                <Route path="/app/chat">
                  <h2>Feature not ready yet :(</h2>
                </Route>
                <Route path="/app/calendar">
                  <h2>Feature not ready yet :(</h2>
                </Route>
                <Route path="/app/teams">
                  <Teams />
                </Route>
                <Route path="/app/settings">
                  <Settings />
                </Route>
              </Switch>
            </main>
          </div>
        </Route>
        <Route path="/login">
          <BasicHeader />
          <Login />
        </Route>
      </Switch>
      {user ? <Redirect to="/app/home" /> : <Redirect to="/login" />}
    </Fragment>
  );
}

export default App;
