import React, { useRef, useEffect } from "react";

/**
 * Hook that alerts clicks outside of the passed ref
 */
const useOutsideAlerter = (props, ref) => {
  useEffect(() => {
    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        props.onLeave();
      }
    }
    // Bind the event listener
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [ref]);
};

/**
 * Component that calls onLeave={functionToCall()}
 * when clicked outside wrapped component
 */
const OutsideAlerter = (props) => {
  const wrapperRef = useRef(null);
  useOutsideAlerter(props, wrapperRef);

  return <div ref={wrapperRef}>{props.children}</div>;
};

export default OutsideAlerter;
