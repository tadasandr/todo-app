import React, { useRef, useEffect, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { authenticateUser, registerUser } from "../../store/AuthStore";
import Button from "../../UI/Button";

import { ToastContainer, toast } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

import { reset } from "../../store/AuthStore";

import "./LoginForm.css";

import Card from "../../UI/Card";
import LoadingSpinner from "../../UI/LoadingSpinner";

const LoginForm = (props) => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const realNameRef = useRef();
  const rpasswordRef = useRef();

  const history = useHistory();
  const dispatch = useDispatch();
  const { user, loading, isError, isSuccess, authMode } = useSelector(
    (state) => state.auth
  );

  const loginHandler = (event) => {
    event.preventDefault();

    if (authMode === "LOGIN") {
      dispatch(
        authenticateUser({
          email: emailRef.current.value,
          password: passwordRef.current.value,
        })
      );
    } else if (authMode === "SIGNUP") {
      dispatch(
        registerUser({
          name: realNameRef.current.value,
          email: emailRef.current.value,
          password: passwordRef.current.value,
        })
      );
    }
  };

  useEffect(() => {
    if (isError) {
      toast.error("Invalid username or password.");
    }

    if (isSuccess || user) {
      history.replace("/app/home");
    }

    dispatch(reset());
  }, [isError, isSuccess, user, dispatch, reset]);

  return (
    <Fragment>
      <ToastContainer />
      <form className="login__form" method="POST">
        <Card className="login-wrapper">
          <h1>{authMode}</h1>
          <div className="input-wrapper">
            {authMode === "SIGNUP" ? (
              <div className="input-wrapper-item">
                <label htmlFor="realName">Name</label>
                <input ref={realNameRef} type="text" name="realName" />
              </div>
            ) : null}
            <div className="input-wrapper-item">
              <label htmlFor="username">Email</label>
              <input ref={emailRef} type="email" name="username" />
            </div>
            <div className="input-wrapper-item">
              <label htmlFor="password">Password</label>
              <input ref={passwordRef} type="password" name="password" />
            </div>
            {authMode === "SIGNUP" ? (
              <div className="input-wrapper-item">
                <label htmlFor="rpassword">Repeat Password</label>
                <input ref={rpasswordRef} type="password" name="rpassword" />
              </div>
            ) : null}
          </div>

          <div className="login-buttons">
            <Button onClick={loginHandler} type="submit">
              {loading ? <LoadingSpinner className="loading-spinner-small" /> : authMode}
            </Button>
          </div>
        </Card>
      </form>
    </Fragment>
  );
};

export default LoginForm;
