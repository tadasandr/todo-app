import React from "react";
import { useSelector } from "react-redux/es/exports";

import Card from "../../UI/Card";
import "./TeamInfo.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCircleInfo,
  faPlus,
  faUserGear,
} from "@fortawesome/free-solid-svg-icons";
import MemberList from "./MemberList";

const TeamInfo = () => {
  const teamSelector = useSelector((state) => state.data.retrievedTeam);
  const user = useSelector((state) => state.auth.user);

  return (
    <>
      {teamSelector && (
        <Card className="teaminfo-wrapper">
          <h2>
            <FontAwesomeIcon icon={faCircleInfo} /> Team Information
          </h2>
          <button className="top-right-corner__button">
            <FontAwesomeIcon icon={faPlus} />
          </button>
          <h4>Members</h4>
          <MemberList list={teamSelector.users} />
          <h4>Admins</h4>
          <div className="creator__div">
            <FontAwesomeIcon icon={faUserGear} />
            <span>{teamSelector.creator.name}</span>
            <span>{teamSelector.creator.email}</span>
          </div>
        </Card>
      )}
    </>
  );
};

export default TeamInfo;
