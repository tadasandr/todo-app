import React from "react";
import Card from "../../UI/Card";

import './Announcements.css'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBullhorn,
  faPlus
} from "@fortawesome/free-solid-svg-icons";


const Announcements = (props) => {

  return (
    <div className="announcements-wrapper">
      <Card>
        <h2><FontAwesomeIcon icon={faBullhorn} /> Announcements</h2>
        <button className="top-right-corner__button"><FontAwesomeIcon icon={faPlus} /></button>
        <h4>No announcements yet.</h4>
      </Card>
    </div>
  );
};

export default Announcements;
