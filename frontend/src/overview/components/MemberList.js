import React from "react";
import Member from "./Member";

import "./MemberList.css";

const MemberList = (props) => {
  return (
    <ul className="memberlist-wrapper">
      {props.actions
        ? props.list &&
          props.list.map((user) => (
            <Member
              actions
              key={user._id}
              name={user.name}
              email={user.email}
            />
          ))
        : props.list &&
          props.list.map((user) => (
            <Member key={user._id} name={user.name} email={user.email} />
          ))}
    </ul>
  );
};

export default MemberList;
