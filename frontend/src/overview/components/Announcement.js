import React from "react";

import userImg from '../../assets/user_placeholder.jpg';

import './Announcement.css';

const Announcement = () => {

    return(
        <div className="announcement-wrapper">
            <div className="announcement-header">
                <img src={userImg} />
                <h4>{props.username}</h4>
            </div>
        </div>
    )
}

export default Announcement;