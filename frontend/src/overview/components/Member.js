import React from "react";

import "./Member.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

const Member = (props) => {
  return (
    <li className="member__li">
      <FontAwesomeIcon icon={faUser} />
      <span>{props.name}</span>
      <span>{props.email}</span>
    </li>
  );
};

export default Member;
