import React, { useEffect, useState } from "react";

import { closeCreateAnnouncementModal, openCreateAnnouncementModal } from "../../store/UIStore";

import Announcements from "../components/Announcements";
import LoadingSpinner from "../../UI/LoadingSpinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPeopleGroup
} from "@fortawesome/free-solid-svg-icons";
import "./Overview.css";

import { useDispatch, useSelector } from "react-redux/es/exports";
import { getUserTeams } from "../../store/DataStore";
import TeamInfo from "../components/TeamInfo";
import Modal from "../../UI/Modal";
import { Link } from "react-router-dom";
import Reload from "../../UI/Reload";

const Overview = () => {
  const teamSelector = useSelector((state) => state.data.retrievedTeam);
  const teamsSelector = useSelector((state) => state.data.teams);
  const userIdSelector = useSelector((state) => state.auth.user._id);
  const modalSelector = useSelector((state) => state.modal.isCreateAnnoucenemtModalOpen);
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    const getTeams = async () => {
      try {
        setIsLoading(true);
        setError(false);
        dispatch(getUserTeams(userIdSelector));
      } catch (error) {
        setError(true);
        setIsLoading(false);
      }
      setIsLoading(false);
    };
    if(teamsSelector.length === 0) {
      getTeams();
    }
  }, []);

  return (
    <>
      <Modal isOpen={modalSelector}/>
      <h1>{teamSelector && <><FontAwesomeIcon icon={faPeopleGroup}/> {teamSelector.teamName} <Reload/></>}</h1>
      {isLoading && <LoadingSpinner />}
      {teamSelector && (
        <div className="overview-wrapper">
          <Announcements />
          <TeamInfo />
        </div>
      )}
      {(!teamSelector && teamsSelector.length === 0) && <h2>Welcome! Let's start by searching for a team or creating one <Link to="/app/teams">here.</Link></h2>}
    </>
  );
};

export default Overview;
