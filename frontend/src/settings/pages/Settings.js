import React from "react";
import { useSelector } from "react-redux";
import GeneralSettings from "../components/GeneralSettings";
import RequestsPanel from "../components/RequestsPanel";

import "./Settings.css";

const Settings = () => {
  const teamSelector = useSelector((state) => state.data.retrievedTeam);

  return (
    <>
      {!teamSelector ? (
        <h2>Team selection is required to use Settings.</h2>
      ) : (
        <div className="projects-wrapper">
          <GeneralSettings />
          <RequestsPanel />
        </div>
      )}
    </>
  );
};

export default Settings;
