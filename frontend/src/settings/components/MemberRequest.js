import React from "react";

import "./MemberRequest.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faCheck, faXmark } from "@fortawesome/free-solid-svg-icons";
import { requestsService } from "../../services/requestsService";

const MemberRequest = (props) => {
  const denyRequestHandler = () => {
    requestsService.denyJoinRequest(props.userId);
  };

  const approveRequestHandler = () => {
    requestsService.approveJoinRequest(props.userId);
  };

  return (
    <li className="member__li">
      <FontAwesomeIcon icon={faUser} />
      <span>{props.name}</span>
      <span>{props.email}</span>

      <div className="actions__div">
        <button onClick={approveRequestHandler}>
          <FontAwesomeIcon className="success-color" icon={faCheck} />
        </button>
        <button onClick={denyRequestHandler}>
          <FontAwesomeIcon className="error-color" icon={faXmark} />
        </button>
      </div>
    </li>
  );
};

export default MemberRequest;
