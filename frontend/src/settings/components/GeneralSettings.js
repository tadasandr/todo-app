import React from "react";
import Card from "../../UI/Card";

import "./GeneralSettings.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGear } from "@fortawesome/free-solid-svg-icons";
import Button from '../../UI/Button';
import { useRef } from "react";
import { ToastContainer, toast } from "react-toastify";
import teamService from "../../services/teamsService";
import { useEffect } from "react";
import { useSelector } from "react-redux";


const GeneralSettings = () => {

  const teamSettings = useSelector((state) => state.data.retrievedTeam.settings);
  const teamId = useSelector((state) => state.data.retrievedTeam._id);

  const setting1 = useRef(teamSettings.onlyCreatorCanInvite);
  const setting2 = useRef(teamSettings.onlyCreatorCanModifyTasks);
  const setting3 = useRef(teamSettings.onlyCreatorCanEditTasks);
  const setting4 = useRef(teamSettings.onlyCreatorCanDeleteTasks);
  const setting5 = useRef(teamSettings.teamIsInvisible);

  const notifySaved = () => {
    toast.info("Settings updated successfully.")
  }

  const saveSettings = () => {
    const settings = {
      onlyCreatorCanInvite: setting1.current.value,
      onlyCreatorCanModifyTasks: setting2.current.value,
      onlyCreatorCanEditTasks: setting3.current.value,
      onlyCreatorCanDeleteTasks: setting4.current.value,
      teamIsInvisible: setting5.current.value
    }
    teamService.saveSettings(settings, teamId);
    notifySaved();
  }

  return (
    <Card className="adminpanel-wrapper">
      <ToastContainer />
      <h2>
        <FontAwesomeIcon icon={faGear} /> General Settings
      </h2>
      <div className="settings-wrapper">
        <div>
          <input ref={setting1} type="checkbox" defaultChecked={teamSettings.onlyCreatorCanInvite} name="creatorInvite" />
          <label htmlFor="creatorInvite">
            Only administators can invite people
          </label>
        </div>
        <div>
          <input ref={setting2} defaultChecked={teamSettings.onlyCreatorCanModifyTasks} type="checkbox" name="creatorModifyTasks" />
          <label htmlFor="creatorModifyTasks">
            Only administators can modify tasks
          </label>
        </div>
        <div>
          <input ref={setting3} defaultChecked={teamSettings.onlyCreatorCanEditTasks} type="checkbox" name="creatorEditTasks" />
          <label htmlFor="creatorEditTasks">
            Only administators can edit tasks
          </label>
        </div>
        <div>
          <input ref={setting4} type="checkbox" defaultChecked={teamSettings.onlyCreatorCanDeleteTasks} name="creatorDeleteTasks" />
          <label htmlFor="creatorDeleteTasks">
            Only administators can delete tasks
          </label>
        </div>
        <div>
          <input ref={setting5} type="checkbox" defaultChecked={teamSettings.teamIsInvisible} name="teamIsVisible" />
          <label htmlFor="teamIsVisible">Is team visible to search</label>
        </div>
        <Button onClick={saveSettings} className="settings-save__button">Save settings</Button>
      </div>
    </Card>
  );
};

export default GeneralSettings;
