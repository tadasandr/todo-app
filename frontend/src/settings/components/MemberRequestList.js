import React from "react";
import MemberRequest from "./MemberRequest";

import "./MemberRequestList.css";

const MemberRequestList = (props) => {
  return (
    <ul className="memberlist-wrapper">
      {props.list &&
        props.list.map((user) => (
          <MemberRequest
            actions
            key={user._id}
            userId={user._id}
            name={user.name}
            email={user.email}
          />
        ))}
        {props.list.length === 0 && <h4>No join requests yet.</h4>}
    </ul>
  );
};

export default MemberRequestList;
