import React from "react";
import { useSelector } from "react-redux/es/hooks/useSelector";

import Card from "../../UI/Card";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { ToastContainer, toast } from "react-toastify";

import "./AdminPanel.css";
import MemberRequestList from "./MemberRequestList";
import Reload from "../../UI/Reload";

const RequestsPanel = () => {
  const requestsSelector = useSelector(
    (state) => state.data.retrievedTeam.joinRequests
  );

  return (
    <Card className="adminpanel-wrapper">
      <ToastContainer />
      <h2>
        <FontAwesomeIcon icon={faUserPlus} /> Join requests
        <Reload />
      </h2>
      <MemberRequestList actions list={requestsSelector} />
    </Card>
  );
};

export default RequestsPanel;
