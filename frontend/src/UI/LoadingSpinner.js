import React from "react";

import "./LoadingSpinner.css";

const LoadingSpinner = (props) => {

  const styles = `loading-spinner ${props.className}`

  return (
    <div className="spinner-container">
      <div className={styles}></div>
    </div>
  );
};

export default LoadingSpinner;
