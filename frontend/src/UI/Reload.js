import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faRotateRight } from "@fortawesome/free-solid-svg-icons";
import taskService from "../services/tasksService";

const Reload = () => {
    
  const reloadTeam = () => {
    taskService.refetchTeam();
  };

  return (
    <button onClick={reloadTeam} className="nostyle__button">
      <h2>
        <FontAwesomeIcon icon={faRotateRight} />
      </h2>
    </button>
  );
};

export default Reload;
