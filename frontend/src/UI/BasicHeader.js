import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { authModeLogin, authModeSignup } from "../store/AuthStore";
import { Link } from "react-router-dom";

import './BasicHeader.css';
import Button from "./Button";

const BasicHeader = (props) => {

    const modeSelector = useSelector((state) => state.auth.authMode);
    const dispatch = useDispatch();

    return(
        <header className="basic_header">
            <h1>PLANNER-TEAMS</h1>
            
            {modeSelector === "LOGIN" ? <Button onClick={() => dispatch(authModeSignup())} className="register_link">Create account</Button> : <Button onClick={() => dispatch(authModeLogin())} className="register_link">Sign In</Button>}
        </header>
    )
}

export default BasicHeader;