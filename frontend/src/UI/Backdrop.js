import { faTachographDigital } from "@fortawesome/free-solid-svg-icons";
import React, { useState } from "react";
import ReactDOM from "react-dom" 

import "./Backdrop.css";

const Backdrop = ({ children }) => {

  const backdrop = <div className="backdrop">{children}</div>;

  return ReactDOM.createPortal(backdrop, document.getElementById("backdrop"));
};

export default Backdrop;
