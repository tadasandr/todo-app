import React from "react";

import taskService from "../services/tasksService";
import "./SelectionBox.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const SelectionBox = (props) => {
  const markAs = (event, status) => {
    event.preventDefault();
    taskService.changeTaskStatus({ taskId: props.taskId, status });
  };

  const deleteTask = () => {
    taskService.removeTask(props.taskId);
  };

  return (
    <div className="dropdown">
      <FontAwesomeIcon icon={faBars} />
      <div className="dropdown-content">
        {props.status === "PROGRESS" || props.status === "DONE" ? (
          <button onClick={(event) => markAs(event, "TODO")}>
            Mark as Todo
          </button>
        ) : null}
        {props.status === "TODO" || props.status === "DONE" ? (
          <button onClick={(event) => markAs(event, "PROGRESS")}>
            Mark as In-progress
          </button>
        ) : null}
        {props.status === "TODO" || props.status === "PROGRESS" ? (
          <button onClick={(event) => markAs(event, "DONE")}>
            Mark as Done
          </button>
        ) : null}
        <button className="dropdown-edit">
          EDIT
        </button>
        <button onClick={deleteTask} className="dropdown-delete">
          DELETE
        </button>
      </div>
    </div>
  );
};

export default SelectionBox;
