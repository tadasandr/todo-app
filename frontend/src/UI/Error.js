import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

import './Error.css';

const Error = (props) => {

    return(
        <div className="error-box">
            <FontAwesomeIcon className="error__icon" icon={faExclamationTriangle}/>
            {props.children}
        </div>
    )
}

export default Error;