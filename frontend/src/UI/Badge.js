import React from "react";

import './Badge.css';

const Badge = (props) => {

    return (
        <span className={`badge ${props.className}`}>{props.children}</span>
    );
}

export default Badge;