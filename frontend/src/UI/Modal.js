import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import Backdrop from "./Backdrop";

import 'animate.css';
import "./Modal.css";

const ModalComponent = (props) => {
  return (
    <div className="modal-content">
      <div className="modal-header">{props.header}</div>
      <div className="modal-body">{props.children}</div>
      <div className="modal-footer">{props.footer}</div>
    </div>
  );
};

const Modal = (props) => {

  const modal = (

    <div className={`modal ${props.className}`}>
        <Fragment>
          <Backdrop />
          <ModalComponent header={props.header} footer={props.footer}>
            {props.children}
          </ModalComponent>
        </Fragment>
    </div>
  );

  return props.isOpen ? ReactDOM.createPortal(modal, document.getElementById("modal")) : null;
};

export default Modal;
