import React, { Fragment, useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux/es/exports";
import { openCreateTaskModal, closeCreateTaskModal } from "../../store/UIStore";
import taskService from "../../services/tasksService";

import Card from "../../UI/Card";
import ProjectItemList from "../components/ProjectItemList";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faListCheck,
  faSpinner,
  faClipboardList,
} from "@fortawesome/free-solid-svg-icons";

import "./Projects.css";
import LoadingSpinner from "../../UI/LoadingSpinner";
import Modal from "../../UI/Modal";
import AddTaskForm from "../components/AddTaskForm";
import Error from "../../UI/Error";
import Reload from "../../UI/Reload";

const Projects = () => {
  const modalSelector = useSelector(
    (state) => state.modal.isCreateTaskModalOpen
  );

  const {
    retrievedTeam,
    todoTasks,
    progressTasks,
    doneTasks,
    isError,
    isLoading,
  } = useSelector((state) => state.data);

  const dispatch = useDispatch();

  const handleOpenModal = () => {
    dispatch(openCreateTaskModal());
  };

  return (
    <Fragment>
      {/* modal and backdrop */}

      <Modal
        className="animate__animated animate__bounceIn"
        isOpen={modalSelector}
      >
        <AddTaskForm modalHandler />
      </Modal>

      {/* modal and backdrop end*/}

      <div className="additional-data">
        {retrievedTeam ? (
          <button className="add-task__button" onClick={handleOpenModal}>
            Add a task
          </button>
        ) : (
          <button
            disabled
            className="add-task__button-disabled"
          >
            Add a task
          </button>
        )}
        {!isLoading && <Reload/>}
        {!retrievedTeam && <Error>No team selected.</Error> }
        {isError ? (
          <Error>Failed to load tasks. Please try again later.</Error>
        ) : null}
        {isLoading ? <LoadingSpinner /> : null}
      </div>
      <div className="projects-wrapper">
        <Card className="projects-card">
          <h2>
            <FontAwesomeIcon icon={faClipboardList} /> Todo
          </h2>

          {retrievedTeam && <ProjectItemList items={todoTasks} />}
          {todoTasks.length === 0 && <h4>No tasks in Todo.</h4>}
        </Card>
        <Card className="projects-card">
          <h2>
            <FontAwesomeIcon icon={faSpinner} /> In-progress
          </h2>

          {retrievedTeam && <ProjectItemList items={progressTasks} />}
          {progressTasks.length === 0 && <h4>No tasks in Progress.</h4>}
        </Card>
        <Card className="projects-card">
          <h2>
            <FontAwesomeIcon icon={faListCheck} /> Done
          </h2>

          {retrievedTeam && <ProjectItemList items={doneTasks} />}
          {doneTasks.length === 0 && <h4>No tasks in Done.</h4>}
        </Card>
      </div>
    </Fragment>
  );
};

export default Projects;
