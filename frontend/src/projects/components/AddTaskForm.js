import React, { Fragment, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeCreateTaskModal } from "../../store/UIStore";
import taskService from "../../services/tasksService";
import { ToastContainer, toast } from "react-toastify";

import "../../teams/components/AddTeamForm.css";

const AddTaskForm = (props) => {
  const retrievedTeam = useSelector((state) => state.data.retrievedTeam);
  const dispatch = useDispatch();

  const taskName = useRef();
  const tagRef = useRef();
  const descriptionRef = useRef();

  const createTaskHandler = (event) => {
    event.preventDefault();

    if (retrievedTeam) {
      taskService.saveTaskToATeam({
        title: taskName.current.value,
        type: tagRef.current.value,
        description: descriptionRef.current.value,
        teamId: retrievedTeam._id,
      });
    } else {
      toast.error("You have to select a team before executing this action.");
    }

    dispatch(closeCreateTaskModal());
  };

  const closeModal = (event) => {
    event.preventDefault();

    dispatch(closeCreateTaskModal());
  };

  return (
    <Fragment>
      <ToastContainer />
      <form className="team__form">
        <label>Task title:</label>
        <input ref={taskName} type="text" name="teamName" />
        <label>Description</label>
        <textarea
          ref={descriptionRef}
          type="text"
          name="description"
        ></textarea>
        <label>Tag:</label>
        <input ref={tagRef} name="tag"></input>
        <button onClick={createTaskHandler}>Create</button>
        <button onClick={closeModal}>Cancel</button>
      </form>
    </Fragment>
  );
};

export default AddTaskForm;
