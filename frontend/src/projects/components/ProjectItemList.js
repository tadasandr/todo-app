import React from "react";
import ProjectItem from "./ProjectItem";

import "./ProjectItemList.css";

const ProjectItemList = (props) => {
  return (
    <ul>
      {props.items.map((i) => (
        <ProjectItem key={i._id} taskId={i._id} title={i.title} description={i.description} type={i.type} status={i.status} createdAt={i.createdAt} lastUpdate={i.updatedAt} />
      ))}
    </ul>
  );
};

export default ProjectItemList;
