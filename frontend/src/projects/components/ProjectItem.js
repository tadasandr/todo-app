import React, { useState } from "react";

import Badge from "../../UI/Badge";
import SelectionBox from "../../UI/SelectionBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";

import "./ProjectItem.css";

const ProjectItem = (props) => {
  const [isDescShown, setIsDescShown] = useState(false);

  const creationDate = props.createdAt.substr(0, 10);
  const updateDate = props.lastUpdate.substr(0, 10);

  const descriptionText = props.description;
  const description = descriptionText.length === 0 ? "No additional content." : props.description;

  return (
    <li className="project-item-wrapper">
      <div className="vl"></div>
      <h4>
        {props.title}{" "}
        {isDescShown ? (
          <button
            className="description__button"
            onClick={() => setIsDescShown(false)}
          >
            <FontAwesomeIcon icon={faChevronDown} />
          </button>
        ) : (
          <button
            className="description__button"
            onClick={() => setIsDescShown(true)}
          >
            <FontAwesomeIcon icon={faChevronUp} />
          </button>
        )}
      </h4>
      <Badge className="list-badge">{props.type}</Badge>
      <SelectionBox status={props.status} taskId={props.taskId} />
      {isDescShown && (
        <>
          <span className="description__span">{description}</span>
          <div className="created-at__div">
            <span>Created: </span><br/>
            <span>Updated: </span>
          </div>
          <div className="updated-at__div">
            <span>{creationDate}</span><br/>
            <span>{updateDate}</span>
          </div>
        </>
      )}
    </li>
  );
};

export default ProjectItem;
