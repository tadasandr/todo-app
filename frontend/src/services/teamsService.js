import axios from "axios";

const API_URL = "http://localhost:5000/api/teams";

const authConfig = () => {
  let token = JSON.parse(localStorage.getItem("user")).token;
  return { headers: { Authorization: `Bearer ${token}` } };
};

// get teams
const getTeams = async () => {
  const response = await axios.get(
    "http://localhost:5000/api/user/teams",
    authConfig()
  );
  return response.data;
};

// create team
const createTeam = async (teamData, userId) => {
  const data = { ...teamData, userId };
  const response = await axios.post(API_URL, data, authConfig());

  return response.data;
};

// get one team
const getTeam = async (teamId) => {
  const response = await axios.post(
    "http://localhost:5000/api/team",
    {
      teamId,
    },
    authConfig()
  );

  return response.data;
};

// search teams

const searchTeams = async (searchTerm) => {
  const response = await axios.post(
    `${API_URL}/search`,
    { searchTerm },
    authConfig()
  );

  return response.data;
};

// leave team
const leaveTeam = async (teamId, userId) => {
  const response = await axios.patch(
    `${API_URL}/users/leave`,
    {
      teamId,
      userId,
    },
    authConfig()
  );
  return response;
};

const saveSettings = async (settings, teamId) => {
  const response = await axios.patch(
    `${API_URL}/settings`,
    { settings, teamId},
    authConfig()
  );
  return response;
};

const teamService = {
  getTeams,
  getTeam,
  createTeam,
  searchTeams,
  leaveTeam,
  saveSettings
};

export default teamService;
