import axios from "axios";
import store from "../store/UIStore";
import tasksService from "../services/tasksService";

const API_URL = "http://localhost:5000/api/requests";

const authConfig = () => {
  let token = JSON.parse(localStorage.getItem("user")).token;
  return { headers: { 'Authorization': `Bearer ${token}` } }
}

const getUser = () => {
  return store.getState().auth.user;
};
const teamId = () => {
  return store.getState().data.retrievedTeam._id;
};

// DENY JOIN REQUEST
// PARAM userId to identify denied user
const denyJoinRequest = async (userId) => {
  await axios.patch(`${API_URL}/join/deny`, { teamId: teamId(), userId }, authConfig());
  tasksService.refetchTeam();
};

const approveJoinRequest = async (userId) => {
  await axios.patch(`${API_URL}/join/approve`, { teamId: teamId(), userId }, authConfig());
  tasksService.refetchTeam();
};

const requestToJoin = async (teamId) => {
  const user = getUser();
  await axios.patch(`${API_URL}/join`, {
    teamId,
    userId: user._id,
    name: user.name,
    email: user.email,
  }, authConfig());
};

export const requestsService = {
  denyJoinRequest,
  requestToJoin,
  approveJoinRequest,
};
