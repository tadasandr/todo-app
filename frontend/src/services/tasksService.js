import axios from "axios";
import { getTeam } from "../store/DataStore";
import store from "../store/UIStore";

const API_URL = "http://localhost:5000/api/teams/tasks";

const authConfig = () => {
  let token = JSON.parse(localStorage.getItem("user")).token;
  return { headers: { 'Authorization': `Bearer ${token}` } }
}

const saveTaskToATeam = async (taskData) => {
  const response = await axios.patch(API_URL, taskData, authConfig());
  refetchTeam();
  return response.data;
};

const changeTaskStatus = async (taskData) => {
  const teamId = store.getState().data.retrievedTeam._id;
  console.log(teamId);
  const response = await axios.patch(`${API_URL}/status`, {
    teamId,
    ...taskData,
  }, authConfig());
  refetchTeam();
  return response.data;
};

const removeTask = async (taskId) => {
  const teamId = store.getState().data.retrievedTeam._id;
  const response = await axios.patch(`${API_URL}/remove`, { teamId, taskId }, authConfig());
  refetchTeam();
  return response.data;
};

const refetchTeam = () => {
  store.dispatch(getTeam(store.getState().data.retrievedTeam._id));
};

const taskService = {
  saveTaskToATeam,
  changeTaskStatus,
  removeTask,
  refetchTeam
};

export default taskService;
