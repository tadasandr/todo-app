import React from "react";

import "../../UI/SelectionBox.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import teamService from "../../services/teamsService";
import { getUserTeams } from "../../store/DataStore";

const TeamsSelectionBox = (props) => {
  const userId = useSelector((state) => state.auth.user._id);
  const dispatch = useDispatch();

  const handleLeaveTeam = () => {
    teamService.leaveTeam(props.teamId, userId);
    dispatch(getUserTeams());
  };

  return (
    <div className="dropdown">
      <FontAwesomeIcon icon={faBars} />
      <div className="dropdown-content">
        <button onClick={handleLeaveTeam} className="error-color">
          LEAVE
        </button>
      </div>
    </div>
  );
};

export default TeamsSelectionBox;
