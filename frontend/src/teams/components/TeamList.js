import React from "react";
import "./TeamList.css";

import Team from "./Team";

const TeamList = (props) => {

  return (
    <ul className="teams-grid">
      {props.items.map((team) => (
        <Team key={team._id} teamId={team._id} team={team} />
      ))}
    </ul>
  );
};

export default TeamList;
