import React from "react";

import Card from "../../UI/Card";
import teamImg from "../../assets/team.png";

import "./Team.css";
import TeamSelectionBox from "./TeamsSelectionBox";

const Team = (props) => {
  return (
    <li className="team-item">
      <Card className="team-card">
        <img className="team__img" src={teamImg} />
        <h2 className="team_name">{props.team.teamName}</h2>
        <TeamSelectionBox teamId={props.teamId} />
      </Card>
    </li>
  );
};

export default Team;
