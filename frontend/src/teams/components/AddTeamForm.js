import React, { useRef } from "react";
import { useDispatch } from "react-redux/";
import { closeCreateTeamModal } from "../../store/UIStore";

import teamService from "../../services/teamsService";
import { useSelector } from "react-redux";

import "./AddTeamForm.css";

const AddTeamForm = () => {
  const dispatch = useDispatch();
  const userIdSelector = useSelector((state) => state.auth.user._id);

  const teamNameRef = useRef();
  const descriptionRef = useRef();

  const createTeamHandler = (event) => {
    event.preventDefault();
    teamService.createTeam({
      teamName: teamNameRef.current.value,
      description: descriptionRef.current.value,
    }, userIdSelector);

    closeModal();
  };

  const closeModal = () => {
    dispatch(closeCreateTeamModal());
  };

  return (
    <form method="POST" className="team__form">
      <label>Team name:</label>
      <input ref={teamNameRef} type="text" name="teamName" />
      <label>Description:</label>
      <textarea ref={descriptionRef} name="description"></textarea>
      <button onClick={createTeamHandler}>Create</button>
      <button onClick={closeModal}>Cancel</button>
    </form>
  );
};

export default AddTeamForm;
