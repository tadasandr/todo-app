import React, { Fragment, useEffect, useState } from "react";
import Button from "../../UI/Button";
import Modal from "../../UI/Modal";
import AddTeamForm from "../components/AddTeamForm";
import { useSelector, useDispatch } from "react-redux";
import { getUserTeams } from "../../store/DataStore";
import TeamList from "../components/TeamList";
import { openCreateTeamModal } from "../../store/UIStore";
import LoadingSpinner from "../../UI/LoadingSpinner";
import Error from "../../UI/Error";

const Teams = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);

  const modalSelector = useSelector((state) => state.modal.isCreateTeamModalOpen);
  const teamsSelector = useSelector((state) => state.data.teams);
  const userIdSelector = useSelector((state) => state.auth.user._id);

  const dispatch = useDispatch();

  useEffect(() => {
    const getTeams = async () => {
      try {
        setIsLoading(true);
        setError(false);
        dispatch(getUserTeams(userIdSelector));
      } catch (error) {
        setError(true);
        setIsLoading(false);
      }
      setIsLoading(false);
    };
    if(teamsSelector.length  === 0) {
      getTeams();
    }
  }, []);

  const openModal = () => {
    dispatch(openCreateTeamModal());
  };

  return (
    <Fragment>
      <Modal
        isOpen={modalSelector}
        className="team_modal animate__animated animate__bounceIn"
      >
        <AddTeamForm />
      </Modal>
      <h1>Your Teams manager.</h1>
      <div className="additional-data">
        <Button onClick={openModal}>Create a team</Button>
        {error ? (
          <Error>Failed to load Teams. Please try again later.</Error>
        ) : null}
        {isLoading ? <LoadingSpinner /> : null}
      </div>
      {teamsSelector.length !== 0 ? (
        <TeamList items={teamsSelector} />
      ) : (
        <h2>No teams joined so far. Maybe search for one?</h2>
      )}
    </Fragment>
  );
};

export default Teams;
