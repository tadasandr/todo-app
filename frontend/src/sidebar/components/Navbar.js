import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHouse,
  faChartSimple,
  faMessage,
  faCalendar,
  faListCheck,
  faArrowRightFromBracket,
  faPeopleGroup,
  faUsersGear,
} from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { logoutUser, reset, removeUser } from "../../store/AuthStore";
import { resetData } from "../../store/DataStore";
import { useHistory } from "react-router-dom";
import { openLoginModal } from "../../store/UIStore";
import "./Navbar.css";

import NavbarItem from "./NavbarItem";

const Navbar = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const teamSelector = useSelector((state) => state.data.retrievedTeam);

  const modalToggleHandler = () => {
    dispatch(openLoginModal());
  };

  const logoutHandler = () => {
    dispatch(logoutUser());
    dispatch(removeUser());
    dispatch(resetData());
    dispatch(reset());
    history.replace("/login");
  };

  return (
    <div className="navbar-wrapper">
      <NavbarItem className="navbar-logo">
        <NavLink to="/app/">
          <h1>TODO-TEAMS</h1>
        </NavLink>
      </NavbarItem>
      <div className="navbar-item-list">
        <NavbarItem>
          <NavLink to="/app/home" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faHouse} />
            Home
          </NavLink>
        </NavbarItem>
        <NavbarItem>
          <NavLink to="/app/tasks" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faListCheck} />
            Tasks
          </NavLink>
        </NavbarItem>
        <NavbarItem>
          <NavLink to="/app/teams" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faPeopleGroup} />
            Teams
          </NavLink>
        </NavbarItem>

        {/* THESE WILL COME LATER */}

        {/* <NavbarItem>
          <NavLink to="/app/chat" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faMessage} />
            Chat <sup>Soon!</sup>
          </NavLink>
        </NavbarItem>
        <NavbarItem>
          <NavLink to="/app/stats" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faChartSimple} />
            Stats <sup>Soon!</sup>
          </NavLink>
        </NavbarItem>
        <NavbarItem>
          <NavLink to="/app/calendar" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faCalendar} />
            Calendar <sup>Soon!</sup>
          </NavLink>
        </NavbarItem> */}
      </div>

      <div className="navbar-bottom-list">
        <NavbarItem>
          <NavLink to="/app/settings" activeClassName="active">
            <FontAwesomeIcon className="font-awesome" icon={faUsersGear} />
            Team Settings
          </NavLink>
        </NavbarItem>
        <NavbarItem onClick={logoutHandler}>
          <NavLink to="/login">
            <FontAwesomeIcon
              className="font-awesome"
              icon={faArrowRightFromBracket}
            />
            Log out
          </NavLink>
        </NavbarItem>
      </div>
    </div>
  );
};

export default Navbar;
