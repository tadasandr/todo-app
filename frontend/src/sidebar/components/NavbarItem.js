import React from "react";

import './NavbarItem.css';

const NavbarItem = props => {

    return(
        <div onClick={props.onClick} className={`navbar-item ${props.className}`}>
            <div>{props.children}</div>
        </div>
    );
};

export default NavbarItem;