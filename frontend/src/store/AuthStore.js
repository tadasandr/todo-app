import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import authService from "../services/authService";

// Get user from localStorage
const user = JSON.parse(localStorage.getItem("user"));

const initialState = {
  user: user ? user : null,
  loading: false,
  isError: false,
  isSuccess: false,
  isLoggedIn: false,
  authMode: "LOGIN",
};

export const authenticateUser = createAsyncThunk(
  "auth/authenticateUser",
  async (user, thunkAPI) => {
    try {
      return await authService.login(user);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const registerUser = createAsyncThunk(
  "auth/registerUser",
  async (userData, thunkAPI) => {
    try {
      return await authService.register(userData);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const logoutUser = createAsyncThunk("auth/logoutUser", async () => {
  await authService.logout();
});

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    reset: (state) => {
      state.isError = false;
      state.loading = false;
      state.isSuccess = false;
      state.isLoggedIn = false;
    },
    removeUser: (state) => {
      state.user = null;
    },
    authModeSignup: (state) => {
      state.authMode = "SIGNUP";
    },
    authModeLogin: (state) => {
      state.authMode = "LOGIN";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(authenticateUser.pending, (state) => {
        state.loading = true;
      })
      .addCase(authenticateUser.fulfilled, (state, action) => {
        state.loading = false;
        state.isSuccess = true;
        state.user = action.payload;
        state.isLoggedIn = true;
      })
      .addCase(authenticateUser.rejected, (state) => {
        state.loading = false;
        state.isError = true;
        state.user = null;
      })
      .addCase(registerUser.fulfilled, (state, action) => {
        state.loading = false;
        state.isSuccess = true;
        state.user = action.payload;
        state.isLoggedIn = true;
      })
      .addCase(registerUser.pending, (state) => {
        state.loading = true;
      })
      .addCase(registerUser.rejected, (state) => {
        state.loading = false;
        state.isError = true;
        state.user = null;
      });
  },
});

export const { reset, removeUser, authModeLogin, authModeSignup } =
  authSlice.actions;
