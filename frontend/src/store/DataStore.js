import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import teamService from "../services/teamsService";

const initialState = {
  teams: [],
  selectedTeam: null,
  retrievedTeam: null,
  isError: false,
  isLoading: false,
  todoTasks: [],
  progressTasks: [],
  doneTasks: [],
};

export const getUserTeams = createAsyncThunk(
  "data/getTeams",
  async (thunkAPI) => {
    try {
      const response = await teamService.getTeams();
      return response;
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const getTeam = createAsyncThunk(
  "data/getTeam",
  async (teamId, thunkAPI) => {
    try {
      return await teamService.getTeam(teamId);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const dataSlice = createSlice({
  name: "data",
  initialState,
  reducers: {
    changeSelectedTeam: (state, action) => {
      state.selectedTeam = action.payload;
      console.log(action.payload);
      localStorage.setItem("activeTeam", action.payload._id);
    },
    resetData: (state) => {
      Object.assign(state, initialState);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getUserTeams.fulfilled, (state, action) => {
        state.teams = action.payload;
        state.isError = false;
      })
      .addCase(getTeam.fulfilled, (state, action) => {
        state.retrievedTeam = action.payload;
        state.todoTasks = action.payload.tasks.filter(
          (task) => task.status === "TODO"
        );
        state.progressTasks = action.payload.tasks.filter(
          (task) => task.status === "PROGRESS"
        );
        state.doneTasks = action.payload.tasks.filter(
          (task) => task.status === "DONE"
        );
        state.isError = false;
        state.isLoading = false;
      })
      .addCase(getTeam.rejected, (state, action) => {
        state.retrievedTeam = action.payload;
        state.isError = true;
        state.isLoading = false;
      })
      .addCase(getTeam.pending, (state, action) => {
        state.retrievedTeam = action.payload;
        state.isLoading = true;
        state.isError = false;
      });
  },
});

export const { changeSelectedTeam, resetData } = dataSlice.actions;
