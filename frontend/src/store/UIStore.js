import { createSlice, configureStore } from "@reduxjs/toolkit";
import { authSlice } from "./AuthStore";
import { dataSlice } from "./DataStore";

const initialState = {
  isLoginModalOpen: true,
  isCreateTeamModalOpen: false,
  isCreateTaskModalOpen: false,
  isCreateAnnoucenemtModalOpen: false,
};

// creating slice

const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    openLoginModal: (state) => {
      state.isLoginModalOpen = true;
    },
    closeLoginModal: (state) => {
      state.isLoginModalOpen = false;
    },
    openCreateTeamModal: (state) => {
      state.isCreateTeamModalOpen = true;
    },
    closeCreateTeamModal: (state) => {
      state.isCreateTeamModalOpen = false;
    },
    openCreateTaskModal: (state) => {
      state.isCreateTaskModalOpen = true;
    },
    closeCreateTaskModal: (state) => {
      state.isCreateTaskModalOpen = false;
    },
    openCreateAnnouncementModal: (state) => {
      state.isCreateAnnoucenemtModalOpen = true;
    },
    closeCreateAnnouncementModal: (state) => {
      state.isCreateAnnoucenemtModalOpen = false;
    },
  },
});

const store = configureStore({
  reducer: {
    modal: modalSlice.reducer,
    auth: authSlice.reducer,
    data: dataSlice.reducer,
  },
});

export const {
  openLoginModal,
  closeLoginModal,
  openCreateTeamModal,
  closeCreateTeamModal,
  openCreateTaskModal,
  closeCreateTaskModal,
  openCreateAnnouncementModal,
  closeCreateAnnouncementModal,
} = modalSlice.actions;

export default store;
