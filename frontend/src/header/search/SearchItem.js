import React from "react";
import { useSelector } from "react-redux";

import teamImg from "../../assets/team.png";
import Button from "../../UI/Button";
import { requestsService } from "../../services/requestsService";

import "./SearchItem.css";

const SearchItem = (props) => {
  const teamsSelector = useSelector((state) => state.data.teams);
  const isInThisTeam = teamsSelector.find((team) => team._id === props.teamId);


  const joinTeamHandler = () => {
    requestsService.requestToJoin(props.teamId);
  };

  return (
    <li className="search_item">
      <img src={teamImg} />
      <span className="search-item__span">{props.teamName}</span>
      <div className="float-right">
        {isInThisTeam ? (
          <span className="search-item__span">Joined</span>
        ) : (
          <button onClick={joinTeamHandler} className="small__button">Request join</button>
        )}
      </div>
    </li>
  );
};

export default SearchItem;
