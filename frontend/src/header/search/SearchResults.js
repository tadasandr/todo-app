import React, { useState } from "react";
import Card from "../../UI/Card";
import SearchItem from "./SearchItem";
import "./SearchResults.css";

const SearchResults = (props) => {
  return (
    <Card className="search-results-wrapper">
      <ul>
        {props.items.length === 0 ? (
          <SearchItem teamId="404" teamName="No results found." />
        ) : (
          props.items.map((item) => (
            <SearchItem
              key={item._id}
              teamId={item._id}
              teamName={item.teamName}
            />
          ))
        )}
      </ul>
    </Card>
  );
};

export default SearchResults;
