import React, { useEffect, useState, useRef, Fragment, useMemo } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleRight } from "@fortawesome/free-regular-svg-icons";

import "./Search.css";
import SearchResults from "./SearchResults";
import { useQuery } from "../../hooks/custom-hooks";
import teamService from "../../services/teamsService";
import OutsideAlerter from "../../hooks/OutsideAlerter";

const Search = (props) => {
  const [searchValue, setSearchValue] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [showResults, setShowResults] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const isMounted = useRef(false);
  const query = useQuery();

  const searchHandler = (e) => {
    setSearchValue(e.target.value);
  };

  useEffect(() => {
    if (isMounted.current) {
      const searchTimeout = setTimeout(async () => {
        const teamsFound = await teamService.searchTeams(searchValue);
        setSearchResults(teamsFound);
        setShowResults(true);
      }, 1000);

      return () => {
        clearTimeout(searchTimeout);
      };
    }
  }, [searchValue]);

  return (
    <Fragment>
      <div className="search-flex">
        <div className="search-wrapper">
          <FontAwesomeIcon
            className="icon__input"
            icon={faArrowAltCircleRight}
          />
          <input
            autoComplete="off"
            onFocus={() => (isMounted.current = true)}
            className="search-input"
            placeholder="Search teams"
            type="text"
            name="search"
            onChange={searchHandler}
          />
        </div>
        {showResults ? (
          <OutsideAlerter onLeave={() => setShowResults(false)}>
            <SearchResults items={searchResults} />
          </OutsideAlerter>
        ) : null}
      </div>
    </Fragment>
  );
};

export default Search;
