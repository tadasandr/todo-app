import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeSelectedTeam, getTeam } from "../../store/DataStore";

import "./Profile.css";
import imagePlaceholder from "../../assets/user_placeholder.png";

const Profile = (props) => {
  const user = useSelector((state) => state.auth.user);
  const teams = useSelector((state) => state.data.teams);
  const { selectedTeam } = useSelector((state) => state.data);

  const dispatch = useDispatch();

  const teamOnChangeHandler = (event) => {
    const selected = teams.find((team) => team._id === event.target.value);
    dispatch(changeSelectedTeam(selected));
    dispatch(getTeam( selected._id ));
  };

  useEffect(() => {
    const selectTeamByDefault = () => {
      if (teams.length > 0) {
        dispatch(changeSelectedTeam(teams[0]));
        dispatch(getTeam(teams[0]._id ));
      }
    };

    selectTeamByDefault();
  }, [teams]);

  return (
    <div className="profile-wrapper">
      <select onChange={teamOnChangeHandler} className="profile__select">
        {selectedTeam ? null : <option>Select a team...</option>}
        {teams.map((team) => (
          <option key={team._id} value={team._id}>
            {team.teamName}
          </option>
        ))}
      </select>
      <span className="profile__span">
        {user ? user.name : "please log in"}
      </span>
      <img src={imagePlaceholder} className="profile__img" />
    </div>
  );
};

export default Profile;
