const express = require("express");
const connectDb = require("./config/db");
const dotenv = require("dotenv");
const cors = require("cors");
const app = express();
dotenv.config();

// MIDDLEWARE IMPORTS

const authMiddleware = require("./middleware/authMiddleware");
const teamMiddleware = require("./middleware/teamMiddleware");

// Routes imports

const projectRoutes = require("./routes/projectsRoutes");
const userRoutes = require("./routes/userRoutes");
const teamRoutes = require("./routes/teamsRoutes");
const requestRoutes = require("./routes/requestsRoutes");

// Adding port to listen to

const port = process.env.PORT || 5000;

// connecting to DB

connectDb();

// Parse incoming data to json

app.use(express.json());

// Handle CORS

const corsOptions = {
  origin: "*",
  methods: ["GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS"],
  allowedHeaders: [
    "Content-Type",
    "Authorization",
    "X-Requested-With",
    "device-remember-token",
    "Access-Control-Allow-Origin",
    "Origin",
    "Accept",
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Methods",
  ],
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

// AUTHENTICATION ROUTES BEFORE AUTH PROTECT MIDDLEWARE

app.use("/api", userRoutes);

// USE AUTH MIDDLEWARE

app.use("/api", authMiddleware.protect);

// USE GET TEAM OBJECT MIDDLEWARE

app.use("/api", teamMiddleware.protect);

// Using defined routes

app.use("/api", requestRoutes);
app.use("/api", projectRoutes);
app.use("/api", teamRoutes);

// Handle unexpected errors

app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500);
  res.json({ message: error.message || "An unknown error occurred!" });
});

// Start listening to port

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
