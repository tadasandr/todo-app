const mongoose = require("mongoose");

const taskSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, "Please add a title"],
  },
  type: {
    type: String,
    required: [true, "Invalid type"],
  },
  description: {
    type: String
  }
});

module.exports = mongoose.model("tasks", taskSchema);
