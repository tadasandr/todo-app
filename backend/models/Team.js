const mongoose = require("mongoose");

// CHILD USER SCHEMA

const userSchema = mongoose.Schema({
  name: {
    type: String,
  },
  email: {
    type: String,
  },
});

// CHILD ANNOUNCMENT SCHEMA

const announcementSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Please add a title"],
    },
    content: {
      type: String,
      required: [true, "Please add content to your announcement"],
    },
    creator: userSchema,
  },
  {
    timestamps: true,
  }
);

// CHILD TASK SCHEMA

const taskSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, "Please add a title"],
    },
    status: {
      type: String,
      default: "TODO",
      required: [true, "Task status is required"],
    },
    type: {
      type: String,
      required: [true, "Invalid type"],
    },
    description: {
      type: String,
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    modifiedLastBy: userSchema,
  },
  {
    timestamps: true,
  }
);

// PARENT schema

const teamSchema = mongoose.Schema(
  {
    teamName: {
      type: String,
      required: [true, "Please type a team name"],
    },
    description: {
      type: String,
    },
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "users",
    },
    settings: {
      onlyCreatorCanInvite: {
        type: Boolean,
        default: true,
      },
      onlyCreatorCanModifyTasks: {
        type: Boolean,
        default: false,
      },
      onlyCreatorCanEditTasks: {
        type: Boolean,
        default: false,
      },
      onlyCreatorCanDeleteTasks: {
        type: Boolean,
        default: true,
      },
      teamIsInvisible: {
        type: Boolean,
        default: true,
      }
    },
    admins: [userSchema],
    users: [userSchema],
    tasks: [taskSchema],
    joinRequests: [userSchema],
    announcements: [announcementSchema],
  },
  {
    timestamps: true,
  }
);
teamSchema.index({ teamName: "text" });

module.exports = mongoose.model("teams", teamSchema);
