const express = require('express');
const requestsController = require('../controllers/requestsController');

const router = express.Router();

// POST

router.patch('/requests/join', requestsController.requestToJoinTheTeam);
router.patch('/requests/join/deny', requestsController.denyTeamJoinRequest);
router.patch('/requests/join/approve', requestsController.approveTeamJoinRequest);


module.exports = router;