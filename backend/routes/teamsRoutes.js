// express imports
const express = require("express");
const router = express.Router();

// controller imports
const teamController = require("../controllers/teamsController");

// GET

router.get("/user/teams", teamController.getTeamsByUser);

// POST

router.post("/teams", teamController.addATeam);
router.post("/team", teamController.getOneTeam);
router.post("/teams/search", teamController.searchTeams);

// UPDATE

router.patch("/teams/users", teamController.addAnUserToATeam);
router.patch("/teams/users/leave", teamController.deleteUserFromATeam);
router.patch("/teams/settings", teamController.changeSettings);

// TEST

router.get("/teams/error", teamController.errorTest);

module.exports = router;
