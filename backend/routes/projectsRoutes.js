const express = require('express');
const router = express.Router();

const tasksController = require('../controllers/tasksController');

// PATCH

router.patch('/teams/tasks', tasksController.saveATaskToTeam)
router.patch('/teams/tasks/status', tasksController.updateTaskStatus);

// DELETE

router.patch('/teams/tasks/remove', tasksController.findAndDeleteTask);

module.exports = router;