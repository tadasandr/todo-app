const teamModel = require("../models/Team");
const asyncHandler = require("express-async-handler");

const protect = asyncHandler(async(req, res, next) => {

    const { teamId } = req.body;

    try {
        req.team = await teamModel.findOne({ _id: teamId });
        next();
    } catch(error) {
        res.status(404);
        throw new Error('Team not found');
    }
})

module.exports = { protect };