const jwt = require('jsonwebtoken')
const asyncHandler = require('express-async-handler')
const userModel = require('../models/User')

const protect = asyncHandler(async (req, res, next) => {
  let token

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    try {
      // Get token from header
      token = req.headers.authorization.split(' ')[1]

      // Verify token
      const decoded = jwt.verify(token, process.env.JWT_SECRET)

      // Get user from the token
      req.user = await userModel.findById(decoded.id).select('-password')

      next()
    } catch (error) {
      console.log(error)
      console.log("USER NOT AUTHORIZED");
      res.status(401)
      throw new Error('Not authorized')
    }
  }

  if (!token) {
    res.status(401)
    console.log("USER NOT AUTHORIZED");
    throw new Error('Not authorized, no token')
  }
})

module.exports = { protect }