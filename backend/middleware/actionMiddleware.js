const userModel = require("../models/User");
const asyncHandler = require("express-async-handler");
const teamModel = require("../models/Team");
var mongoose = require("mongoose");

/*
    Checks if the user is part of the team so only the users
    that are in the team can perform actions.
*/
const actionsProtect = asyncHandler(async (req, res, next) => {
  const { teamId, userId } = req.body;

  try {
    const team = await teamModel.findOne({ _id: teamId });
    const containsUser = team.users.find(
      (user) => user._id.toString() === userId
    );

    if (!containsUser) {
      throw new Error("Not authorized.");
    }

    next();
  } catch (error) {
    throw new Error("Not authorized.");
  }
});

module.exports = { actionsProtect };
