const asyncHandler = require("express-async-handler");
const teamModel = require("../models/Team");

/*
    REQUIRES team and user object to check.
    CHECKS if given user is an admin.
*/
const checkForAdmin = asyncHandler(async (req, res, next) => {
  
  const { teamId } = req.body;

  try {
    const team = await teamModel.findOne({ _id: teamId });
    
  } catch(error) {

  }

  const user = req.user;
  const admins = req.team.admins;

  const userMatch = admins.find((u) => u._id === user._id);

  if (userMatch) {
    return next();
  }

  res.status(403);
  throw new Error("User is not authorized");
});

module.exports = { checkForAdmin };
