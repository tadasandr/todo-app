const teamModel = require("../models/Team");
const userModel = require("../models/User");
const mongoose = require("mongoose");

// teamname creator
// @desc    create a team
// @route   POST /api/teams
// @access  Private
const addATeam = async (req, res) => {
  const { teamName, description } = req.body;
  const userId = req.user._id;
  let findCreator = null;

  if (mongoose.Types.ObjectId.isValid(userId)) {
    findCreator = await userModel.findById(userId, "_id name email");
  } else {
    throw new Error("_id is not a valid ObjectId type");
  }

  if (!findCreator) {
    res
      .status(404)
      .json({ message: "User trying to create this team does not exist" });
  }

  const createdTeam = await teamModel.create({
    teamName,
    description,
    creator: findCreator._id,
    users: [findCreator],
  });

  if (createdTeam) {
    return res.status(201).json(createdTeam);
  } else {
    res.status(404);
    throw new Error("Failed to create the team");
  }
};

// @desc    get teams by user id
// @route   POST /user/teams
// @access  Private
const getTeamsByUser = async (req, res) => {
  const userId = req.user._id;

  const teamsByUser = await teamModel
    .find({ users: { $elemMatch: { _id: userId } } })
    .select("_id teamName");

  if (!teamsByUser) {
    res.status(404);
    return res.json({ message: "No teams found containing this user" });
  }

  res.json(teamsByUser);
};

// @desc    Add user to a team
// @route   UPDATE /api/teams/users
// @access  Private
const addAnUserToATeam = async (req, res) => {
  const { user } = req.body;
  const teamId = req.team._id;

  teamModel.findOneAndUpdate(
    { _id: teamId },
    { $push: { users: user } },
    (error, success) => {
      if (error) {
        res.json({ message: error });
      } else {
        res.status(201).json({ message: success });
      }
    }
  );
};

// @desc    delete user from a team
// @route   UPDATE /api/teams/users
// @access  Private
const deleteUserFromATeam = async(req, res) => {
  const { userId } = req.body;
  const teamId = req.team._id;

  try {
    await teamModel
    .updateOne(
      {
        _id: teamId,
      },
      { $pull: { users: { _id: userId } } },
    )
    .clone();
    res.status(200).json({message: "Successfully removed user from team"})
  } catch(error) {
    res.status(500).json(error)
  }
}

// @desc    get teams
// @route   POST /api/team
// @access  Private
const getOneTeam = async (req, res) => {
  const teamId = req.team._id;

  const response = await teamModel
    .findById({ _id: teamId })
    .populate("creator")
    .select(" -__v");

  res.status(200).json(response);
};

// @desc    search for teams
// @route   POST /api/teams/search
// @access  Private
const searchTeams = async (req, res) => {
  const searchTerm = req.body.searchTerm;

  if (searchTerm === "") {
    return res.status(400).json({ message: "Search string is empty." });
  }

  const response = await teamModel
    .find({ teamName: { $regex: searchTerm, $options: "i" }, "settings.teamIsInvisible": false})
    .limit(8);

  if (!response) {
    res.status(200).error({ message: "No teams found." });
  }

  res.status(200).json(response);
};

// @desc    add an announcement
// @route   POST /api/teams/announcements/add
// @access  Private
const addAnAnnouncement = async(req, res) => {
  const { title, content } = req.body;
  const teamId = req.team._id;
  const userId= req.user._id;

  const user = await userModel.find({ _id: userId }).select("-password");
  const team = await teamModel.find({_id: teamId});


}

// @desc    change team settings
// @route   PATCH /api/teams/settings 
// @access  Private
const changeSettings = async(req, res) => {
  const { settings } = req.body;
  const team = req.team;

  console.log(team);

  try {
    await teamModel.updateOne({ _id: team._id }, { $set: { settings } });
    res.status(200).json({ message: "Updated." });
  } catch(error) {
    throw new Error(error.message);
  }
}

const errorTest = () => {
  throw new Error("You got an error lmao")
}

module.exports = {
  addATeam,
  searchTeams,
  getTeamsByUser,
  addAnUserToATeam,
  getOneTeam,
  deleteUserFromATeam,
  changeSettings,
  errorTest
};
