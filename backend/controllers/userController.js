const userModel = require("../models/User");
const bcrypt = require("bcryptjs");
const asyncHandler = require("express-async-handler");
const jwt = require("jsonwebtoken");

// @desc    register user
// @route   POST /api/register
// @access  Public
const register = asyncHandler(async (req, res) => {
  // retrieve data from frontend inputs
  const { name, email, password } = req.body;

  if (!name || !email || !password) {
    res.status(400);
    throw new Error("Please fill all the inputs");
  }

  // check if user already exists
  const userExists = await userModel.findOne({ email });

  if (userExists) {
    res.status(409);
    throw new Error("User already exists");
  }

  // hash password
  const salt = await bcrypt.genSalt(10);
  const encryptedPassword = await bcrypt.hash(password, salt);

  // create user

  const createdUser = await userModel.create({
    name,
    email,
    password: encryptedPassword,
  });

  if (createdUser) {
    res.status(201).json({
      _id: createdUser.id,
      name: createdUser.name,
      email: createdUser.email,
      token: generateToken(createdUser._id),
    });
  } else {
    res.status(400);
    throw new Error("Invalid user data");
  }
});

const login = asyncHandler(async (req,res) => {

    //get credentials from inputs
    const { email, password } = req.body;

    // check if email exists
    const userExists = await userModel.findOne({ email });
    if(!userExists) {
        res.status(404);
        throw new Error('User by this email does not exist');
    }

    if(await bcrypt.compare(password, userExists.password)) {
        res.json({
            _id: userExists.id,
            name: userExists.name,
            email: userExists.email,
            token: generateToken(userExists._id)
        })
    } else {
        res.status(400);
        throw new Error('User does not exist');
    }
});

const generateToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: "30d",
  });
};

module.exports = {
  register,
  login
};
