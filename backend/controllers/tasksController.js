const taskModel = require("../models/Task");
const teamModel = require("../models/Team");
const validTaskStatuses = ["TODO", "PROGRESS", "DONE"];

// @desc    save a Task to a Team document
// @route   PATCH /api/teams/tasks
// @access  Private
const saveATaskToTeam = async (req, res) => {
  const { title, type, description, teamId } = req.body;

  // find team

  const findTeam = await teamModel.findOne({ _id: teamId });

  if (!findTeam) {
    return res.json(404).json({ message: "Team not found..." });
  }

  const newTask = new taskModel({ title, type, description });

  const response = await findTeam.update({ $push: { tasks: newTask } });

  res.status(201).json(response);
};

// @desc    update task status
// @route   PATCH /api/teams/tasks/status
// @access  Private
const updateTaskStatus = async (req, res) => {
  const { teamId, taskId, status } = req.body;

  console.log(req.body);
  console.log(req.body.teamId);

  if (!validTaskStatuses.includes(status)) {
    return res.status(400).json({
      message: "Wrong task status. Valid statuses: TODO/PROGRESS/DONE",
    });
  }

  const findAndUpdateTask = await teamModel
    .updateOne(
      { _id: teamId, "tasks._id": taskId },
      { $set: { "tasks.$.status": status } },
      (err, docs) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          return res.status(200).json(docs);
        }
      }
    )
    .clone()
    .catch(function (err) {
    });

  if (!findAndUpdateTask) {
    return res.status(400).json({ message: "Not found" });
  }
};

// @desc    delete task
// @route   PATCH teams/tasks/remove
// @access  Private
const findAndDeleteTask = async (req, res) => {
  const { teamId, taskId } = req.body;

  await teamModel
    .updateOne({ _id: teamId },
      { $pull: { tasks: { _id: taskId } } },
      (err, docs) => {
        if (err) {
          res.status(200).json({ message: `Task deletion was unsuccessful.` });
        } else {
          res.status(200).json({ message: `Task deleted.` });
        }
      }
    )
    .clone()
    .catch(function (err) {
    });
};

module.exports = {
  saveATaskToTeam,
  updateTaskStatus,
  findAndDeleteTask,
};
