const teamModel = require("../models/Team");
const userModel = require("../models/User");
const mongoose = require("mongoose");

// @desc    request to join to a team
// @route   POST /api/requests/join
// @access  Private
const requestToJoinTheTeam = async (req, res) => {
  const { teamId, name, email } = req.body;
  const userId = req.user._id;

  try {

    const team = await teamModel.findOne({ _id: teamId });
    const alreadyRequested = team.joinRequests.find(j => j._id.toString() === userId.toString());
    console.log(alreadyRequested);
    if(alreadyRequested) {
      throw new Error("This user already requested to join. Request pending...")
    }

    await teamModel.updateOne(
      { _id: teamId },
      { $push: { joinRequests: { _id: userId, name, email } } }
    );

    res.status(200).json({ message: "Join request sent." });
  } catch (error) {
    res.status(500).json(error);
  }
};

// @desc    deny request to join the team
// @route   POST /api/requests/join/deny
// @access  Private
const denyTeamJoinRequest = (req, res) => {
  const { teamId, userId } = req.body;

  removeUserFromJoinRequests(teamId, userId, res);
};

// @desc    approve request to join the team
// @route   POST /api/requests/join/approve
// @access  Private
const approveTeamJoinRequest = async (req, res) => {
  const { teamId, userId } = req.body;

  const user = await userModel.findOne({ _id: userId });

  if (!user) {
    res.status(400).json({ message: "User not found" });
  }
  try {
    await teamModel.updateOne({ _id: teamId }, { $push: { users: user } }).clone();
  } catch (error) {
    res.status(500).json(error);
  }

  removeUserFromJoinRequests(teamId, userId, res);
};

// @desc helper function since this is used more than once in this file.
const removeUserFromJoinRequests = async (teamId, userId, res) => {
  try {
    await teamModel
      .updateOne(
        {
          _id: teamId,
        },
        { $pull: { joinRequests: { _id: userId } } }
      )
      .clone();
    res
      .status(200)
      .json({ message: "Success" });
  } catch (error) {
    res.status(500).json(error);
  }
};

module.exports = {
  requestToJoinTheTeam,
  denyTeamJoinRequest,
  approveTeamJoinRequest,
};
